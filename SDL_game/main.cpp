#include "Game.h"
#include "core.h"
#include <iostream>

#define DEBUG

int main(int argc, char** argv)
{
	Game* game = new Game();

	game->run();

	return 0;
}
