#include "Ground.h"

Ground::Ground() : GameObject("Ground", "Ground")
{
	contactedPlayer = false;
}

Ground::Ground(string type) : GameObject(type, "Ground")
{
	contactedPlayer = false;
}

Ground::~Ground()
{
}  

void Ground::contactPlayer()
{
	contactedPlayer = true;
}

void Ground::uncontactPlayer()
{
	contactedPlayer = false;
}

bool Ground::isContactPlayer()
{
	return contactedPlayer;
}
