#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <SDL2\SDL.h>
#include <map>

class Keyboard
{
public:
	static Keyboard* Instance();

private:
	Keyboard();
	~Keyboard();

private:
	const Uint8* state;
};

#endif //KEYBOARD_H