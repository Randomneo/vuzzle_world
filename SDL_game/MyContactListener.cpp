#include "MyContactListener.h"
#include <iostream>
#include <stdlib.h>

void MyContactListener::BeginContact(b2Contact* contact)
{
	std::string aType;
	std::string bType;

	
	
	GameObject* a;
	GameObject* b;

	void* userData = contact->GetFixtureA()->GetBody()->GetUserData();
	if (!userData)
		return;
	a = static_cast<GameObject*>(userData);

	userData = contact->GetFixtureB()->GetBody()->GetUserData();
	if (!userData)
		return;
	b = static_cast<GameObject*>(userData);

	Rect ar = a->getRect();
	Rect br = b->getRect();
	if (a->getType() == "Player" && b->getCoreType() == "Ground"
		&& br.y > ar.y + ar.h
		&& br.x - ar.w <= ar.x && br.x + br.w >= ar.x)
	{
		static_cast<Player*>(a)->addGroundContact();
		static_cast<Ground*>(b)->contactPlayer();
	}

	if (a->getType() == "Player" && b->getType() == "Button")
		static_cast<GameButton*>(b)->pressButton();

	
}


void MyContactListener::EndContact(b2Contact* contact)
{
	std::string aType;
	std::string bType;
	

	GameObject* a;
	GameObject* b;

	void* userData = contact->GetFixtureA()->GetBody()->GetUserData();
	if (!userData)
		return;
	a = static_cast<GameObject*>(userData);

	userData = contact->GetFixtureB()->GetBody()->GetUserData();
	if (!userData)
		return;
	b = static_cast<GameObject*>(userData);

	Rect ar = a->getRect();
	Rect br = b->getRect();
	if (a->getType() == "Player" && b->getCoreType() == "Ground"
		&& br.y > ar.y + ar.h
		&& static_cast<Ground*>(b)->isContactPlayer())
		static_cast<Player*>(a)->removeGroundContact();

	if (a->getType() == "Player" && b->getType() == "Button")
		static_cast<GameButton*>(b)->unpressButton();
		
}
