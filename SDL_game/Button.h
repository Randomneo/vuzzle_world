#pragma once
#include <SDL2\SDL.h>
#include "UIObject.h"
#include "Label.h"
#include "core.h"

class Button : public UIObject 
{
public:
	Button();
	Button(Label* _label, Rect _rect);
	~Button();

	void draw();
	void setAnimator(Animator* _animator);
	Label* getLabel();

private:
	Label* label;
};

