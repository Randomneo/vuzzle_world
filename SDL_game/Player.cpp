#include "Player.h"
#include <algorithm>

Player::Player() : GameObject("Player")
{
	onG = false;
	groundContacts = 0;
}

void Player::update(int tikcs)
{
	const Uint8* state = SDL_GetKeyboardState(NULL);
	b2Vec2 t = body->GetLinearVelocity();
	t.x = t.x >= 0 ? min(t.x, MAX_PLAYER_SPEED) : max(t.x, -MAX_PLAYER_SPEED);
	body->SetLinearVelocity(t);

	updateOnGround();

	if (state[SDL_SCANCODE_UP] && onG)
		body->ApplyLinearImpulseToCenter(b2Vec2(0, -20), true);
	if (state[SDL_SCANCODE_LEFT] && (-t.x > -MAX_PLAYER_SPEED || t.x > 0))
		body->ApplyLinearImpulseToCenter(b2Vec2(-3, 0), true);
	else if (state[SDL_SCANCODE_RIGHT] && t.x < MAX_PLAYER_SPEED)
		body->ApplyLinearImpulseToCenter(b2Vec2(3, 0), true);
}

void Player::addGroundContact()
{
	groundContacts++;
}
void Player::removeGroundContact()
{
	groundContacts = max(0, groundContacts - 1);
}
void Player::updateOnGround()
{
	if (groundContacts == 0)
		onG = false;
	else
		onG = true;
}

bool Player::onGround()
{
	return onG;
}

void Player::addToWorld(b2World *world)
{
	b2BodyDef* bodyDef = new b2BodyDef();
	bodyDef->type = b2_dynamicBody;
	bodyDef->position.Set(rect.x + rect.w / 2, rect.y + rect.h / 2);
	bodyDef->fixedRotation = true;
	body = world->CreateBody(bodyDef);

	b2PolygonShape* box = new b2PolygonShape();
	box->SetAsBox(rect.w / 2, rect.h / 2);

	b2FixtureDef* fixtureDef = new b2FixtureDef();
	fixtureDef->shape = box;
	fixtureDef->density = 1.f;
	fixtureDef->friction = 0.1f;
	fixtureDef->filter.categoryBits = PLAYER;
	fixtureDef->filter.maskBits = GROUND | BUTTON;

	body->CreateFixture(fixtureDef);

	body->SetUserData(this);
}
