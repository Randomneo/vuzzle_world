#pragma once
#include <Box2D\Dynamics\Contacts\b2Contact.h>
#include <Box2D\Dynamics\b2WorldCallbacks.h>
#include <string>
#include "GameObject.h"
#include "Player.h"
#include "GameButton.h"
#include "Ground.h"

class MyContactListener :
	public b2ContactListener
{
public:
	MyContactListener() : b2ContactListener() { }
	~MyContactListener() { }

	void BeginContact(b2Contact* contact);
	void EndContact(b2Contact* contact);	
};

