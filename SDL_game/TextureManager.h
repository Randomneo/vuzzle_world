#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include <SDL2/SDL.h>
#include <string>
#include <map>
#include "core.h"

using namespace std;

class TextureManager
{
public:
	TextureManager(SDL_Renderer* _renderer);
	SDL_Texture* genTexture(string filename);

private:
	SDL_Renderer* renderer;
};

#endif//TEXTURE_MANAGER_H
