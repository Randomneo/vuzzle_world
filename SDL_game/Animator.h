#ifndef ANIMATION_H
#define ANIMATION_H

#include <SDL2\SDL.h>
#include <stdlib.h>
#include <string>
#include "core.h"

using namespace std;

class Animator
{
public:
	Animator();
	Animator* getCopy();
	void setTexture(SDL_Texture* _texture);
	void draw(Rect rect, bool drawBorderLines); 
	/*
	Calculate frame
	*/
	void update(int ticks);
	void setRowSize(int _rowSize);
	void setRow(int _row, int _rowSize);
	void setTextureSize(Vector2i size);
	void setTextureSize(SDL_Rect size);
	void setFlip(SDL_RendererFlip _flip);
	/* speed 1 is 1 sprite per 10 ms */
	void setSpeed(float _speed);
	void setNoScale();

private:
	SDL_Renderer* renderer;
	SDL_Texture* texture;
	Rect srcRect; // ret on src file to draw
	Vector2i frame; //frame of row
	int rowSize; // num of frames in row
	SDL_RendererFlip flip;
	float speed;// speed = 1 is 1 sprite per 10 ms
	bool noScale;
};

#endif //ANIMATION_H