#include "GameObject.h"
#include <iostream>

GameObject::GameObject(string _type)
{
	drawLineRect = true;
	type = _type;
	coreType = "NoType";
}

GameObject::GameObject(string _type, string _coreType)
{
	drawLineRect = true;
	type = _type;
	coreType = _coreType;
}

GameObject::~GameObject()
{
	//std::cout << "GameObject destruct;" << std::endl;
	body->GetWorld()->DestroyBody(body);
}

void GameObject::setRect(Rect _rect)
{
	rect = _rect;
}

void GameObject::setRect(float x, float y, float w, float h)
{
	rect.x = x;
	rect.y = y;
	rect.w = w;
	rect.h = h;
}

Rect GameObject::getRect()
{
	return rect;
}
SDL_Rect GameObject::getSDL_Rect()
{
	return rect.getSDL_Rect();
}

void GameObject::setPos(Vector2f newPos)
{
	rect.x = newPos.x;
	rect.y = newPos.y;
}

void GameObject::setAnimator(Animator* _animator)
{
	animator = _animator;
}
Animator* GameObject::getAnimator()
{
	return animator;
}

void GameObject::update(int ticks)
{
	//Change frame
	animator->update(ticks);
	//Aplly new position
	rect.update(body);
}

void GameObject::draw()
{
	animator->draw(rect, drawLineRect);
}

void GameObject::addToWorld(b2World *world)
{
	b2BodyDef* bodyDef = new b2BodyDef();
	bodyDef->type = b2_kinematicBody;
	bodyDef->position.Set(rect.x + rect.w/2, rect.y + rect.h/2);
	body = world->CreateBody(bodyDef);

	b2PolygonShape* box = new b2PolygonShape();
	box->SetAsBox(rect.w / 2, rect.h / 2);

	b2FixtureDef* fixtureDef = new b2FixtureDef();
	fixtureDef->shape = box;
	fixtureDef->density = 1.f;
	fixtureDef->friction = 1.f;
	fixtureDef->filter.categoryBits = GROUND;

	body->CreateFixture(fixtureDef);
	
	body->SetUserData(this);
}

void GameObject::setDrawLineRect(bool t)
{
	drawLineRect = t;
}

string GameObject::getType()
{
	return type;
}
	     
string GameObject::getCoreType()
{
	return coreType;
}
