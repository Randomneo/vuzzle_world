#pragma once
#include "GameObject.h"
#include "Platform.h"

class Action
{
public:
	Action(GameObject* _object,string _name);
	~Action();

	void apply();

private:
	GameObject* object;
	string objectName;
	string name;
};

