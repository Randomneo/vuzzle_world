#pragma once
#include "GameObject.h"
#include "Action.h"
#include <string>

class GameButton : public GameObject
{
public:
	GameButton();
	~GameButton();
	void pressButton();
	void unpressButton();
	void setPressAction(Action* action);
	void setUnpressAction(Action* action);
	void addToWorld(b2World* world);

private:
	Action* pressAction;
	Action* unpressAction;
	bool isOn;
};

