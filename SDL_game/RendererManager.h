#pragma once
#include <SDL2\SDL.h>

class RendererManager
{
private:
	RendererManager();
	~RendererManager();

public:
	static void setRenderer(SDL_Window* window);
	static void setRenderer(SDL_Renderer* _renderer);
	static SDL_Renderer* getRenderer();

private:
	static SDL_Renderer* renderer;
};

