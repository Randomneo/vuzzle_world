#pragma once
#include "GameObject.h"
#include <string>
class Ground : public GameObject
{
public:
	Ground();
	Ground(string type);
	~Ground();

	void contactPlayer();
	void uncontactPlayer();
	bool isContactPlayer();

private:
	bool contactedPlayer;

};

