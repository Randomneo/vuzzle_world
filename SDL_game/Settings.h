#ifndef SETTINGS_H
#define SETTINGS_H

#include "core.h"

#include <string>
#include <fstream>
#include <iostream>

#include <SDL2\SDL.h>

using namespace std;

class Settings
{
public:
	Settings();
	Settings(string filename);

	Rect getWindowRect();
	Uint32 getFullScreenFlag();

private:
	void loadWindowRect();
	void loadFullScreenFlag();
	
	ifstream fin;
	Rect windowRect;
	Uint32 fullScreenFlag;
};

#endif //SETTIGNS_H