#include "Button.h"

Button::Button()
{
}

Button::Button(Label* _label, Rect _rect) : UIObject(&_rect)
{
	_label->offAutoWidth();
	label = _label;
	animator->setTextureSize(_rect.getSDL_Rect());
}

void Button::setAnimator(Animator* _animator)
{
	animator = _animator;
}

Label* Button::getLabel()
{
	return label;
}

void Button::draw()
{
	animator->draw(rect, true);
	label->draw();
}

Button::~Button()
{
}
