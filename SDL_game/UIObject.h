#pragma once
#include <SDL2\SDL.h>
#include <string>
#include "core.h"
#include "Animator.h"

class UIObject
{
public:
	void init();
	UIObject();
	UIObject(Rect* _rect);
	~UIObject();

	void draw();

protected:
	std::string name;
	SDL_Renderer* renderer;
	Animator* animator;
	Rect rect;
	bool drawBorderLines;
};

