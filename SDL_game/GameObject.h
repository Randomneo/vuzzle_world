#pragma once
#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "core.h"
#include <SDL2\SDL.h>
#include <Box2D\Box2D.h>
#include <string>
#include <fstream>
#include "Animator.h"

using namespace std;

class GameObject
{
	
public:	   
	//TODO: move to global file
	enum entityCategory {
		GROUND = 0x0001,
		BUTTON = 0x0002,
		PLAYER = 0x0004
	};
	GameObject(string _type);
	GameObject(string _type, string _coreType);
	virtual ~GameObject();
	void update(int ticks);
	void setRect(Rect _rect);
	void setRect(float x, float y, float w, float h);
	void setPos(Vector2f newPos);
	SDL_Rect getSDL_Rect();
	Rect getRect();
	void setDrawLineRect(bool);
	void draw();
	void setAnimator(Animator* _animator);
	Animator* getAnimator();
	void addToWorld(b2World *world);
	string getType();
	string getCoreType();

protected:
	Rect rect;
	SDL_Renderer* renderer;
	Animator* animator;
private:
	bool drawLineRect;
	string type;
	string coreType;

	//Box2D
public:
	b2Body* body;
};

#endif //GAME_OBJECT_H