#pragma once
#include "GameObject.h"
#include "Ground.h"



class Platform : public Ground
{
	enum drection
	{
		up,
		right,
		down,
		left
	};
public:
	Platform(Rect _rect, float _speed);
	~Platform();
	
	void moveUp();
	void moveDown();
	void moveLeft();
	void moveRight();
	void stop();
	void resume();
	
	void update();

private:
	float speed;
	int prevDir;
	Rect movingRect;
};

