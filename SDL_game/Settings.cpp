#include "Settings.h"

Settings::Settings()
{
}

Settings::Settings(string filename)
{
	fin.open(filename.c_str(), ios::in);

	if (!fin.good()) {
		cout << "Error open file" << endl;
		exit(1);
	}

	string type;

	do {
		fin >> type;
		if (type == "window:")
			loadWindowRect();
		if (type == "fullscreen:")
			loadFullScreenFlag();
	} while (type != "end");
}

Rect Settings::getWindowRect()
{
	return windowRect;
}

Uint32 Settings::getFullScreenFlag()
{
	return fullScreenFlag;
}

void Settings::loadWindowRect()
{
	fin >> windowRect.x >> windowRect.y >>
		windowRect.w >> windowRect.h;
}

void Settings::loadFullScreenFlag()
{
	bool t;
	fin >> t;
	if (t)
		fullScreenFlag = SDL_WINDOW_FULLSCREEN_DESKTOP;
	else
		t = 0;
}