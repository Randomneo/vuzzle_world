#ifndef CORE_H
#define CORE_H

#include <math.h>
#include <SDL2\SDL.h>
#include <Box2D\Box2D.h>

struct Vector2i
{
	int x;
	int y;
	Vector2i() {
		x = 0;
		y = 0;
	}
	Vector2i(int _x, int _y) {
		x = _x;
		y = _y;
	}

	Vector2i operator+ (const Vector2i& v) {
		return Vector2i(x + v.x, y + v.y);
	}
	Vector2i operator- (const Vector2i& v) {
		return Vector2i(x - v.x, y - v.y);
	}
};

struct Vector2f
{
	float x;
	float y;
	Vector2f() {
		x = 0;
		y = 0;
	}
	Vector2f(float _x, float _y) {
		x = _x;
		y = _y;
	}

	Vector2f operator+ (const Vector2f& v) {
		return Vector2f(x + v.x, y + v.y);
	}
	Vector2f operator- (const Vector2f& v) {
		return Vector2f(x - v.x, y - v.y);
	}
	void normilize() {
		float l = sqrt(x + y);
		x = x / l;
		y = y / l;
	}
	Vector2i Int() {
		return Vector2i((int)x, (int)y);
	}
};

/*
Rect with float position
*/
struct Rect
{
	float x;
	float y;
	float w;
	float h;
	Rect()
	{
		x = y = w = h = 0;
	}
	Rect(float _x, float _y, float _w, float _h)
	{
		x = _x;
		y = _y;
		w = _w;
		h = _h;
	}

	SDL_Rect getRectToDraw(float scale)
	{
		SDL_Rect rect;
		rect.x = int(x*scale);
		rect.y = int(y*scale);
		rect.w = int(w*scale);
		rect.h = int(h*scale);
		return rect;
	}

	SDL_Rect getSDL_Rect()
	{
		SDL_Rect rect;
		rect.x = int(x);
		rect.y = int(y);
		rect.w = int(w);
		rect.h = int(h);
		return rect;
	}

	void update(b2Body* body)
	{
		x = body->GetPosition().x - w/2;
		y = body->GetPosition().y - h/2;
	}
};

#endif // CORE_H
