#pragma once
#include <SDL2\SDL.h>
#include <vector>
#include "core.h"
#include "Label.h"

class UIManager
{
private:
	UIManager();
	~UIManager();
	
public:
	static UIManager* get();
	
	void draw();
	void addLabel(Label* label);
	//void addButton(Button* button);

private:
	//vector<UIObjects*> uiObjects;
	static UIManager* UIManager::ui;
};

