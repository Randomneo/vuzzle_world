#include "Game.h"
#include "RendererManager.h"
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <SDL2\SDL_opengl.h>
#include <SDL2\SDL_render.h>
#include <SDL2\SDL_main.h>
#include <SDL2\SDL_ttf.h>

Game::Game()
{
	settings = Settings(SETTINGS_FILE);
	framesCounter = 0;

	initSDL();
	initOpenGL();
	initBox2d();

	textureMgr = new TextureManager(renderer);
	is_running = true;

	fpsLabel = new Label("FPS: ", Rect(0, 0, 100, 35));
	fpsLabel->hide();

	Animator* anim = new Animator();
	anim->setRow(0, 1);
	anim->setTexture(textureMgr->genTexture("../res/uibutton.png"));
	anim->setTextureSize(Rect(0, 0, 2000, 500).getSDL_Rect());
	anim->setNoScale();
	Label* lb = new Label();
	lb->offAutoWidth();
	lb->setRect(Rect(105, 105, 140, 30));
	lb->setText("Button");
	button = new Button(lb, Rect(100, 100, 150, 40));
	button->setAnimator(anim);
	
	loadFile(LEVEL_FILE);
}

bool Game::initSDL()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", "Cant init sdl", NULL);
		exit(1);
	}
	Rect windowRect = settings.getWindowRect();
	window = SDL_CreateWindow(
		WINDOW_TITLE,
		windowRect.x,
		windowRect.y,
		windowRect.w,
		windowRect.h,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | fullScreenFlag);

	if (!window) {
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", "Cant create window", NULL);
		exit(1);
	}
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	RendererManager::setRenderer(renderer);

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

	if (TTF_Init() < 0)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", "Cant load sdl2_ttf", NULL);
		exit(1);
	}

	return true;
}

bool Game::initBox2d()
{
	b2Vec2 gravity(0.f, 10.f);

	world = new b2World(gravity); 
	world->SetContactListener(&contactListener);

	return true;
}

bool Game::initOpenGL()
{
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GLContext context = SDL_GL_CreateContext(window);
	SDL_GL_SetSwapInterval(1);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, 200, 0, 200, -1, 1);
	glMatrixMode(GL_MODELVIEW);

	return true;
}

void Game::render()
{
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	SDL_RenderClear(renderer);


	for (int i = 0; i < gameObjcets.size(); i++)
	{
		gameObjcets[i]->draw();
	}

	fpsLabel->draw();
	button->draw();

	SDL_RenderPresent(renderer);
	//SDL_GL_SwapWindow(window);
}

void Game::run()
{
	while (is_running) {
		events();
		update();
		render();
	}
	clean();
}

void Game::clean()
{
	delete fpsLabel;
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	TTF_Quit();
	SDL_Quit();
}

void Game::events()
{
	//dont create pointer(core dumped renderer == NULL)
	SDL_Event event;
	if (SDL_PollEvent(&event)) {
		switch (event.type)
		{
		case SDL_QUIT:
			is_running = false;
		}
	}
}

void Game::update()
{
	int ticks = SDL_GetTicks();

	world->Step(1.f/(GAME_SPEED * 50.0) * abs(ticks - worldPrevTicks), 8, 3);
	worldPrevTicks = ticks;
	
	for (int i = 0; i < gameObjcets.size(); i++)
	{
		gameObjcets[i]->update(ticks);
	}
	player->update(ticks);
	platform->update();

	framesCounter++;
	if (ticks - prevTicks >= 1000)
	{
		fpsLabel->show();
		stringstream ss("");
		ss << "fps: " << framesCounter;
		fpsLabel->setText(ss.str());
		framesCounter = 0;
		prevTicks = ticks;
	}
}

void Game::loadFile(string filename)
{
	ifstream fin;
	fin.open(filename.c_str(), ios::in);

	gameObjcets = vector<GameObject *>();

	//add player
	Vector2f newHeroPos;
	player = new Player();	
	player->setRect(12.5, 7.5, 2, 4.5/2);

	Animator* animator = new Animator();
	animator->setRow(0, 8);
	animator->setTexture(textureMgr->genTexture(HERO_TEXTURE_FILE));
	animator->setTextureSize(Vector2i(80, 90));

	player->setAnimator(animator);

	animator = NULL;

	player->addToWorld(world);
					     
	gameObjcets.push_back(player);

	Animator* groundAnimator = new Animator();

	groundAnimator->setRow(0, 1);
	groundAnimator->setTexture(textureMgr->genTexture(GROUND_TEXTURE_FILE));
	groundAnimator->setTextureSize(Vector2i(1280, 1280));

	//add ground
	int n;
	fin >> n;
	for (int i = 0; i < n; i++)
	{
		double x, y;
		Ground* ground = new Ground();
		
		fin >> x >> y;
		ground->setRect(x, y, 2.5, 2.5);

		ground->setAnimator(groundAnimator);

		ground->addToWorld(world);
		gameObjcets.push_back(ground);
	}

	//add platform
	platform = new Platform(Rect(0, 0, 15, 15), 1);
	platform->setRect(0, 17.0/2, 2.5, 1.5);
	platform->setAnimator(groundAnimator);
	platform->addToWorld(world);
	gameObjcets.push_back(platform);

	//add button
	Animator* buttonAnimator = new Animator();

	buttonAnimator->setRow(1, 1);
	buttonAnimator->setTexture(textureMgr->genTexture(BUTTON_TEXTURE_FILE));
	buttonAnimator->setTextureSize(Vector2i(160, 160));

	GameButton* button = new GameButton();
	button->setRect(15, 7.5, 2.5, 2.5);
	button->setAnimator(buttonAnimator);
	button->addToWorld(world);

	button->setPressAction(new Action(platform, "resume"));
	button->setUnpressAction(new Action(platform, "stop"));

	gameObjcets.push_back(button);

	delete animator;
	groundAnimator = NULL;
	delete groundAnimator;
}
