#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL.h>
#include <Box2D\Box2D.h>
#include <fstream>
#include <vector>
#include <string>
#include "Settings.h"
#include "GameObject.h"
#include "TextureManager.h"
#include "Player.h"
#include "GameButton.h"
#include "Enemy.h"
#include "Ground.h"
#include "Keyboard.h"
#include "Platform.h"
#include "MyContactListener.h"
#include "Label.h"
#include "Button.h"

using namespace std;

#define HERO_TEXTURE_FILE "../res/hero_spritesheet.png"
#define GROUND_TEXTURE_FILE "../res/ground.png"
#define BUTTON_TEXTURE_FILE "../res/button.png"
#define LEVEL_FILE "../res/level.lv"
#define SETTINGS_FILE "../res/Settings.sts"
#define WINDOW_TITLE "SDL_Game"
//move to settings
#define GAME_SPEED 10

class Game
{
public:
	Game();
	void run();
private:
	bool initOpenGL();
	bool initSDL();
	bool initBox2d();
	void render();
	void update();
	void events();
	void clean();
	void loadFile(string);
	void loadSettings(string);

	bool is_running;
	Settings settings;

	int prevTicks;
	int worldPrevTicks;
	int framesCounter;
	SDL_Window* window;
	SDL_Renderer* renderer;
	Rect windowRect;
	Uint32 fullScreenFlag;

	TextureManager* textureMgr;

	//box2d
	b2World* world;

	//in game objects
	vector<GameObject *> gameObjcets;
	Platform* platform;
	Player* player;
	MyContactListener contactListener;

	Label* fpsLabel;
	Button* button;
};


#endif //GAME_H
