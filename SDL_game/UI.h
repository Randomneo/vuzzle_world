#pragma once
#include <SDL2\SDL.h>
#include <SDL2\SDL_ttf.h>
#include <map>
#include <string>

class UI
{
public:
	UI();
	~UI();

private:
	std::string fontPath;
	std::map<std::string, TTF_Font*> fonts;
};

