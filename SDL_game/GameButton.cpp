#include "GameButton.h"
#include <iostream>
#include <stdlib.h>

GameButton::GameButton() : GameObject("Button")
{
	isOn = false;
}

GameButton::~GameButton()
{
}

void GameButton::pressButton()
{
	isOn = true;
	animator->setRow(0, 1);
	pressAction->apply();
}

void GameButton::unpressButton()
{
	isOn = false;
	animator->setRow(1, 1);
	unpressAction->apply();
}

void GameButton::setPressAction(Action* action)
{
	pressAction = action;
}

void GameButton::setUnpressAction(Action* action)
{
	unpressAction = action;
}

void GameButton::addToWorld(b2World *world)
{
	b2BodyDef* bodyDef = new b2BodyDef();
	bodyDef->type = b2_kinematicBody;
	bodyDef->position.Set(rect.x + rect.w / 2, rect.y + rect.h / 2);
	body = world->CreateBody(bodyDef);

	b2PolygonShape* box = new b2PolygonShape();
	box->SetAsBox(rect.w / 2, rect.h / 2);

	b2FixtureDef* fixtureDef = new b2FixtureDef();
	fixtureDef->shape = box;
	fixtureDef->density = 1.f;
	fixtureDef->friction = 1.f;  
	fixtureDef->isSensor = true;
	fixtureDef->filter.categoryBits = BUTTON;
	//fixtureDef->filter.maskBits = PLAYER;

	body->CreateFixture(fixtureDef);

	body->SetUserData(this);
}