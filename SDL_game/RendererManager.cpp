#include "RendererManager.h"

SDL_Renderer* RendererManager::renderer;

RendererManager::RendererManager()
{
}

RendererManager::~RendererManager()
{
}

void RendererManager::setRenderer(SDL_Window* window)
{
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}
void RendererManager::setRenderer(SDL_Renderer* _renderer)
{
	renderer = _renderer;
}

SDL_Renderer* RendererManager::getRenderer()
{
	return renderer;
}