#ifndef PLAYER_H
#define PLAYER_H

#include "GameObject.h"
#include <SDL2\SDL.h>
#include <Box2D\Box2D.h>

#define MAX_PLAYER_SPEED 5.f

class Player : public GameObject
{
public:
	Player();
	void update(int ticks);
	void addGroundContact();
	void removeGroundContact();
	void updateOnGround();
	int getContacts() { return groundContacts; }
	bool onGround();
	void addToWorld(b2World* world);

private:
	Vector2f speed;
	bool onG;
	int groundContacts;
};

#endif //PLAYER_H