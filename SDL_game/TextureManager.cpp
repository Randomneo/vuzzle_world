#include "TextureManager.h"
#include <SDL2/SDL_image.h>

TextureManager::TextureManager(SDL_Renderer* _renderer)
{
	renderer = _renderer;
}

SDL_Texture* TextureManager::genTexture(string filename)
{
	SDL_Texture* texture;
	SDL_Surface* tmpSurface;
	
	tmpSurface = IMG_Load(filename.c_str());
	texture = SDL_CreateTextureFromSurface(renderer, tmpSurface);
	SDL_FreeSurface(tmpSurface);
	
	return texture;
}