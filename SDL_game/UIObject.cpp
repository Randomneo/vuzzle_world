#include "UIObject.h"

void UIObject::init()
{
	drawBorderLines = true;
}

UIObject::UIObject()
{
	init();
}

UIObject::UIObject(Rect* _rect)
{
	init();
	animator = new Animator();
	animator->setNoScale();
	rect = *_rect;
}

void UIObject::draw()
{
	animator->draw(rect, drawBorderLines);
}

UIObject::~UIObject()
{
	//delete animator;
}
