#include "Action.h"

Action::Action(GameObject* _object, string _name)
{
	object = _object;
	objectName = object->getType();
	name = _name;
}

Action::~Action()
{
}

void Action::apply()
{
	if (objectName == "Platform")
	{
		if (name == "moveUp")
			static_cast<Platform*>(object)->moveUp();
		if (name == "moveDown")
			static_cast<Platform*>(object)->moveDown();
		if (name == "moveRight")
			static_cast<Platform*>(object)->moveRight();
		if (name == "moveLeft")
			static_cast<Platform*>(object)->moveLeft();
		if (name == "stop")
			static_cast<Platform*>(object)->stop();
		if (name == "resume")
			static_cast<Platform*>(object)->resume();
	}
}