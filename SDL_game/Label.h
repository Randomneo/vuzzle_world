#pragma once
#include <string>
#include <SDL2\SDL.h>
#include <SDL2\SDL_ttf.h>
#include "core.h"
#include "Animator.h"
#include "UIObject.h"

class Label : public UIObject
{
public:
	Label();
	Label(std::string _text, Rect _rect);
	~Label();

	void init();

	void setName(std::string _name);
	void setText(std::string _text);
	void setRect(Rect _rect);
	void setColor(SDL_Color _color);
	void setFont(const char* filename, int size);
	void setFont(TTF_Font* _font);
	void setSymbolWidth(int width);
	void onAutoWidth();
	void offAutoWidth();
	void show();
	void hide();

	void draw();

private:
	bool isShow;
	std::string text;
	int symbolWidth;
	bool autoWidth;
	SDL_Surface* surf;
	SDL_Color color;
	TTF_Font* font;
};

