#include "Platform.h"

Platform::Platform(Rect _rect, float _speed = 1) : 
	Ground("Platform"), 
	movingRect(_rect), speed(_speed), prevDir(right)
{
}

Platform::~Platform()
{
}

void Platform::moveUp()
{
	body->SetLinearVelocity(b2Vec2(0, -speed));
	prevDir = up;
}

void Platform::moveDown()
{
	body->SetLinearVelocity(b2Vec2(0, speed));
	prevDir = down;
}

void Platform::moveLeft()
{
	body->SetLinearVelocity(b2Vec2(-speed, 0));
	prevDir = left;
}

void Platform::moveRight()
{
	body->SetLinearVelocity(b2Vec2(speed, 0));
	prevDir = right;
}

void Platform::stop()
{
	body->SetLinearVelocity(b2Vec2(0, 0));
}

void Platform::update()
{
	if (rect.x + rect.w > movingRect.x + movingRect.w)
		moveLeft();
	if (rect.x < movingRect.x)
		moveRight();

	if (rect.y + rect.w > movingRect.y + movingRect.w)
		moveUp();
	if (rect.y < movingRect.y)
		moveDown();
}

void Platform::resume()
{
	switch (prevDir)
	{
	case up: 
		moveUp();
		break;
	case down:
		moveDown();
		break;
	case left:
		moveLeft();
		break;
	case right:
		moveRight();
		break;
	}
}