#include "Label.h"
#include "RendererManager.h"
#include <string>

#define FONT_ARIAL "C:/Windows/Fonts/Arial.ttf"

void Label::init()
{
	font = TTF_OpenFont(FONT_ARIAL, 32);
	if (!font)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", "can't open font", NULL);
		exit(1);
	}
	color = { 0, 255, 0 };
	autoWidth = true;
	isShow = true;
	symbolWidth = 15;
}

Label::Label() : UIObject(&Rect(0,0,100,30))
{
	init();
}

Label::Label(std::string _text, Rect _rect)
	: UIObject(&_rect)
{
	init();
	setText(_text);
}

void Label::setRect(Rect _rect)
{
	rect = _rect;
}

void Label::setName(std::string _name)
{
	name = _name;
}

void Label::setText(std::string _text)
{
	text = _text;

	if (autoWidth)
		rect.w = text.size() * symbolWidth;
	surf = TTF_RenderText_Blended(font, text.c_str(), color);
	SDL_Texture* texture = SDL_CreateTextureFromSurface(RendererManager::getRenderer(), surf);

	animator->setTextureSize(rect.getSDL_Rect());
	animator->setTexture(texture);
}

void Label::setFont(const char* filename, int size = 24)
{
	font = TTF_OpenFont(filename, size);
}
void Label::setFont(TTF_Font* _font)
{
	font = _font;
}

void Label::setColor(SDL_Color _color)
{
	color = _color;
}

void Label::setSymbolWidth(int width)
{
	symbolWidth = width;
}
void Label::onAutoWidth()
{
	autoWidth = true;
}
void Label::offAutoWidth()
{
	autoWidth = false;
}

void Label::draw()
{
	if (isShow)
		animator->draw(rect, drawBorderLines);
}

void Label::show()
{
	isShow = true;
}

void Label::hide()
{
	isShow = false;
}

Label::~Label()
{
	TTF_CloseFont(font);
	SDL_FreeSurface(surf);
	delete animator;
}