#include "Animator.h"
#include "RendererManager.h"
#include <string>
#include <sstream>

Animator::Animator()
{
	renderer = RendererManager::getRenderer();
	speed = 0.1;
	flip = SDL_FLIP_NONE;
	noScale = false;
}

Animator* Animator::getCopy()
{
	Animator* a = new Animator();
	a->setFlip(flip);
	a->setTexture(texture);
	a->setRow(frame.y, rowSize);
	a->setTextureSize(srcRect.getSDL_Rect());
	a->setSpeed(speed);
	return a;
}

void Animator::setTexture(SDL_Texture* _texture)
{
	texture = _texture;
}

void Animator::setRowSize(int _rowSize)
{
	rowSize = _rowSize;
}
void Animator::setRow(int _row, int _rowSize)
{
	frame.y = _row;
	rowSize = _rowSize;
}

void Animator::setTextureSize(Vector2i size)
{
	srcRect.w = float(size.x);
	srcRect.h = float(size.y);
}
void Animator::setTextureSize(SDL_Rect size)
{
	srcRect.w = float(size.w);
	srcRect.h = float(size.h);
}

void Animator::draw(Rect dstRect, bool drawBorderLines)
{
	float scale = 40;
	srcRect.x = srcRect.w * float(frame.x);
	srcRect.y = srcRect.h * float(frame.y);

	if (srcRect.w == 0 || srcRect.h == 0)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", "wrong src rect", NULL);
		return;
	}

	//Create scale
	SDL_Rect t;
	if (noScale)
		t = dstRect.getSDL_Rect();
	else
		t = dstRect.getRectToDraw(scale);
	try
	{
		SDL_RenderCopyEx(renderer, texture, &srcRect.getSDL_Rect(), &t, 0, 0, flip);
	}
	catch (...)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", "Cant draw object", NULL);
	}
	if (drawBorderLines)
	{
		SDL_SetRenderDrawColor(renderer, 0, 255, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderDrawLine(renderer, t.x, t.y, t.x + t.w, t.y);
		SDL_RenderDrawLine(renderer, t.x, t.y, t.x, t.y + t.h);
		SDL_RenderDrawLine(renderer, t.x + t.w, t.y + t.h, t.x + t.w, t.y);
		SDL_RenderDrawLine(renderer, t.x + t.w, t.y + t.h, t.x, t.y + t.h);

		SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	}
}

void Animator::update(int ticks)
{
	frame.x = int(ticks / 10 * speed) % rowSize;
}

void Animator::setFlip(SDL_RendererFlip _flip)
{
	flip = _flip;
}

void Animator::setSpeed(float _speed)
{
	speed = _speed;
}

void Animator::setNoScale()
{
	noScale = true;
}